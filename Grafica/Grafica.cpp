#include <GL/glew.h>
#include <chrono>
#include <iostream>

#include "Display.h"
#include "Skybox.h"
#include "Terrain.h"
int main()
{

	// Display
	Display display(WindowWidth, WindowHeight, "Cheile Gradistei");

	// Camera & Transform
	Camera camera(glm::vec3(41, 5, -40), 70.0f, static_cast<float>(WindowWidth) / WindowHeight, 0.01f, 1000.0f);
	display.SetCamera(camera);
	//Terrain
	Terrain terrain;
	//Skybox
	Skybox skybox;

	//MainRoad
	Mesh* roadM = new Mesh("./resources/road.obj");
	Texture* roadT = new Texture("./resources/road.jpg");
	Shader* roadS = new Shader("./resources/shaders/basicShader");
	Transform transformR(glm::vec3(0.0f,0.1f,0.0f), glm::vec3(0,0,0),glm::vec3(50.0f,4.0f,10.0f));

	//SecondRoad
	Mesh* roadM2 = new Mesh("./resources/road.obj");
	Texture* roadT2 = new Texture("./resources/road.jpg");
	Shader* roadS2 = new Shader("./resources/shaders/basicShader");
	Transform transformR2(glm::vec3(-40.0f, 0.1f, 0.0f), glm::vec3(0, 1.625, 0), glm::vec3(20.0f, 3.0f, 4.0f));

	//ThirdRoad
	Mesh* roadM3 = new Mesh("./resources/road.obj");
	Texture* roadT3 = new Texture("./resources/road.jpg");
	Shader* roadS3 = new Shader("./resources/shaders/basicShader");
	Transform transformR3(glm::vec3(40.0f, 0.1f, 0.0f), glm::vec3(0, 1.625, 0), glm::vec3(20.0f, 3.0f, 4.0f));

	//tree

	Mesh* treeM = new Mesh("./resources/tree2/tree.obj");
	Texture* treeT = new Texture("./resources/tree2/branches.png");
	Shader* treeS = new Shader("./resources/shaders/basicShader");
	Transform transformT(glm::vec3(-2.0f, 0.1f, -15.f), glm::vec3(), glm::vec3(1.5f,1.5f,1.5f));

	//Tree2
	Mesh* treeM2 = new Mesh("./resources/tree2/tree.obj");
	Texture* treeT2 = new Texture("./resources/tree2/branches.png");
	Shader* treeS2 = new Shader("./resources/shaders/basicShader");
	Transform transformT2(glm::vec3(-20.0f, 0.1f, 10.f), glm::vec3(), glm::vec3(1.5f, 1.5f, 1.5f));

	//Tree3
	Mesh* treeM3 = new Mesh("./resources/tree2/tree.obj");
	Texture* treeT3 = new Texture("./resources/tree2/branches.png");
	Shader* treeS3 = new Shader("./resources/shaders/basicShader");
	Transform transformT3(glm::vec3(0.0f, 0.1f, 10.f), glm::vec3(), glm::vec3(1.5f, 1.5f, 1.5f));

	//Tree4
	Mesh* treeM4 = new Mesh("./resources/tree2/tree.obj");
	Texture* treeT4 = new Texture("./resources/tree2/branches.png");
	Shader* treeS4 = new Shader("./resources/shaders/basicShader");
	Transform transformT4(glm::vec3(20.0f, 0.1f, 10.f), glm::vec3(), glm::vec3(1.5f, 1.5f, 1.5f));

	//thuja9
	Mesh* thujaM9 = new Mesh("./resources/altcopac2.obj");
	Texture* thujaT9 = new Texture("./resources/thuja_tex2.png");
	Shader* thujaS9 = new Shader("./resources/shaders/basicShader");
	Transform transformThuja9(glm::vec3(-45.0f, 0.1f, 40.0f), glm::vec3(0, 2.5, 0), glm::vec3(0.2f, 0.2f, 0.2f));
	
	//thuja
	Mesh* thujaM = new Mesh("./resources/altcopac2.obj");
	Texture* thujaT = new Texture("./resources/thuja_tex2.png");
	Shader* thujaS = new Shader("./resources/shaders/basicShader");
	Transform transformThuja(glm::vec3(-50.0f, 0.1f, 40.0f), glm::vec3(0,2.5,0), glm::vec3(0.2f,0.2f,0.2f));

	//thuja2
	Mesh* thujaM2 = new Mesh("./resources/altcopac2.obj");
	Texture* thujaT2 = new Texture("./resources/thuja_tex2.png");
	Shader* thujaS2 = new Shader("./resources/shaders/basicShader");
	Transform transformThuja2(glm::vec3(-55.0f, 0.1f, 40.0f), glm::vec3(0, 2.5, 0), glm::vec3(0.2f, 0.2f, 0.2f));

	//thuja3
	Mesh* thujaM3 = new Mesh("./resources/altcopac2.obj");
	Texture* thujaT3 = new Texture("./resources/thuja_tex2.png");
	Shader* thujaS3 = new Shader("./resources/shaders/basicShader");
	Transform transformThuja3(glm::vec3(-60.0f, 0.1f, 40.0f), glm::vec3(0, 2.5, 0), glm::vec3(0.2f, 0.2f, 0.2f));

	//thuja8
	Mesh* thujaM8 = new Mesh("./resources/altcopac2.obj");
	Texture* thujaT8 = new Texture("./resources/thuja_tex2.png");
	Shader* thujaS8 = new Shader("./resources/shaders/basicShader");
	Transform transformThuja8(glm::vec3(-65.0f, 0.1f, 40.0f), glm::vec3(0, 2.5, 0), glm::vec3(0.2f, 0.2f, 0.2f));

	//thuja4
	Mesh* thujaM4 = new Mesh("./resources/altcopac2.obj");
	Texture* thujaT4 = new Texture("./resources/thuja_tex2.png");
	Shader* thujaS4 = new Shader("./resources/shaders/basicShader");
	Transform transformThuja4(glm::vec3(-65.0f, 0.1f, 35.0f), glm::vec3(0, 2.5, 0), glm::vec3(0.2f, 0.2f, 0.2f));

	//thuja5
	Mesh* thujaM5 = new Mesh("./resources/altcopac2.obj");
	Texture* thujaT5 = new Texture("./resources/thuja_tex2.png");
	Shader* thujaS5 = new Shader("./resources/shaders/basicShader");
	Transform transformThuja5(glm::vec3(-65.0f, 0.1f, 30.0f), glm::vec3(0, 2.5, 0), glm::vec3(0.2f, 0.2f, 0.2f));

	//thuja6
	Mesh* thujaM6 = new Mesh("./resources/altcopac2.obj");
	Texture* thujaT6 = new Texture("./resources/thuja_tex2.png");
	Shader* thujaS6 = new Shader("./resources/shaders/basicShader");
	Transform transformThuja6(glm::vec3(-65.0f, 0.1f, 25.0f), glm::vec3(0, 2.5, 0), glm::vec3(0.2f, 0.2f, 0.2f));

	//thuja7
	Mesh* thujaM7 = new Mesh("./resources/altcopac2.obj");
	Texture* thujaT7 = new Texture("./resources/thuja_tex2.png");
	Shader* thujaS7 = new Shader("./resources/shaders/basicShader");
	Transform transformThuja7(glm::vec3(-65.0f, 0.1f, 20.0f), glm::vec3(0, 2.5, 0), glm::vec3(0.2f, 0.2f, 0.2f));

	//thuja10
	Mesh* thujaM10 = new Mesh("./resources/altcopac2.obj");
	Texture* thujaT10 = new Texture("./resources/thuja_tex2.png");
	Shader* thujaS10 = new Shader("./resources/shaders/basicShader");
	Transform transformThuja10(glm::vec3(-65.0f, 0.1f, 15.0f), glm::vec3(0, 2.5, 0), glm::vec3(0.2f, 0.2f, 0.2f));

	//thuja11
	Mesh* thujaM11 = new Mesh("./resources/altcopac2.obj");
	Texture* thujaT11 = new Texture("./resources/thuja_tex2.png");
	Shader* thujaS11 = new Shader("./resources/shaders/basicShader");
	Transform transformThuja11(glm::vec3(-65.0f, 0.1f, 10.0f), glm::vec3(0, 2.5, 0), glm::vec3(0.2f, 0.2f, 0.2f));

	//Deer
	Mesh* deer = new Mesh("./resources/deer.obj");
	Texture* deerT=new Texture("./resources/deer.jpg");
	Shader* deerS = new Shader("./resources/shaders/basicShader");
	Transform transformD(glm::vec3(-75.f, 0.1f, -27.0f), glm::vec3(0,-0.3f,0), glm::vec3(0.1f, 0.1f, 0.1f));

	//Deer2
	Mesh* deer2 = new Mesh("./resources/deer.obj");
	Texture* deerT2 = new Texture("./resources/deer.jpg");
	Shader* deerS2 = new Shader("./resources/shaders/basicShader");
	Transform transformD2(glm::vec3(-70.f, 0.1f, -32.0f), glm::vec3(), glm::vec3(0.1f, 0.1f, 0.1f));

	//Deer3
	Mesh* deer3 = new Mesh("./resources/deer.obj");
	Texture* deerT3 = new Texture("./resources/deer.jpg");
	Shader* deerS3 = new Shader("./resources/shaders/basicShader");
	Transform transformD3(glm::vec3(-65.f, 0.1f, -30.0f), glm::vec3(0,0.3,0), glm::vec3(0.1f, 0.1f, 0.1f));

	//wolf
	Mesh* wolfM = new Mesh("./resources/WOLF.obj");
	Texture* wolfT = new Texture("./resources/blanita.jpeg");
	Shader* wolfS = new Shader("./resources/shaders/basicShader");
	Transform transformW(glm::vec3(60.0f, 0.1f, 35.0f), glm::vec3(0,0.3,0), glm::vec3(2.0f, 2.0f, 2.0f));

	//wolf2
	Mesh* wolfM2 = new Mesh("./resources/WOLF.obj");
	Texture* wolfT2 = new Texture("./resources/blanita.jpeg");
	Shader* wolfS2 = new Shader("./resources/shaders/basicShader");
	Transform transformW2(glm::vec3(65.0f, 0.1f, 32.0f), glm::vec3(0, 0.3, 0), glm::vec3(2.0f, 2.0f, 2.0f));

	//wolf3
	Mesh* wolfM3 = new Mesh("./resources/WOLF.obj");
	Texture* wolfT3 = new Texture("./resources/blanita.jpeg");
	Shader* wolfS3 = new Shader("./resources/shaders/basicShader");
	Transform transformW3(glm::vec3(70.0f, 0.1f, 30.0f), glm::vec3(0, -0.3, 0), glm::vec3(2.0f, 2.0f, 2.0f));

	//wol4
	Mesh* wolfM4 = new Mesh("./resources/WOLF.obj");
	Texture* wolfT4 = new Texture("./resources/blanita.jpeg");
	Shader* wolfS4 = new Shader("./resources/shaders/basicShader");
	Transform transformW4(glm::vec3(75.0f, 0.1f, 27.0f), glm::vec3(0, -0.3, 0), glm::vec3(2.0f, 2.0f, 2.0f));

	//wol5
	Mesh* wolfM5 = new Mesh("./resources/WOLF.obj");
	Texture* wolfT5 = new Texture("./resources/blanita.jpeg");
	Shader* wolfS5 = new Shader("./resources/shaders/basicShader");
	Transform transformW5(glm::vec3(67.5f, 0.1f, 40.0f), glm::vec3(0, 0.3, 0), glm::vec3 (3.f, 3.f, 3.f));
	
	//casuta

	Shader* casutaS = new Shader("./resources/shaders/basicShader");
	Transform transformC(glm::vec3(10.0f, 0.1f, -13.0f), glm::vec3(0, 0, 0), glm::vec3(1.5f, 1.5f, 1.5f));

	std::vector<Mesh*> casuta;
	casuta.emplace_back(new Mesh("./resources/casuta/balcony.obj"));
	casuta.emplace_back(new Mesh("./resources/casuta/barrels.obj"));
	casuta.emplace_back(new Mesh("./resources/casuta/boards.obj"));
	casuta.emplace_back(new Mesh("./resources/casuta/boards_support.obj"));
	casuta.emplace_back(new Mesh("./resources/casuta/chimneys.obj"));
	casuta.emplace_back(new Mesh("./resources/casuta/cover_window.obj"));
	casuta.emplace_back(new Mesh("./resources/casuta/doors.obj"));
	casuta.emplace_back(new Mesh("./resources/casuta/foundation.obj"));
	casuta.emplace_back(new Mesh("./resources/casuta/gutters.obj"));
	casuta.emplace_back(new Mesh("./resources/casuta/parapets.obj"));
	casuta.emplace_back(new Mesh("./resources/casuta/roof.obj"));
	casuta.emplace_back(new Mesh("./resources/casuta/roof_support.obj"));
	casuta.emplace_back(new Mesh("./resources/casuta/walls1.obj"));
	casuta.emplace_back(new Mesh("./resources/casuta/window_roof.obj"));
	casuta.emplace_back(new Mesh("./resources/casuta/windows.obj"));

	std::vector<Texture*> casutaTexturi;
	casutaTexturi.emplace_back(new Texture("./resources/casuta/textures/concrete_light.png"));
	casutaTexturi.emplace_back(new Texture("./resources/casuta/textures/marble_dark.png"));
	casutaTexturi.emplace_back(new Texture("./resources/casuta/textures/wood_balls_brown.png"));
	casutaTexturi.emplace_back(new Texture("./resources/casuta/textures/wood_balls_brown.png"));
	casutaTexturi.emplace_back(new Texture("./resources/casuta/textures/foundation_brown_brick.png"));
	casutaTexturi.emplace_back(new Texture("./resources/casuta/textures/concrete_brown.png"));
	casutaTexturi.emplace_back(new Texture("./resources/casuta/textures/wood_white.png"));
	casutaTexturi.emplace_back(new Texture("./resources/casuta/textures/foundation_brown_brick.png"));
	casutaTexturi.emplace_back(new Texture("./resources/casuta/textures/plastic_grey.png"));
	casutaTexturi.emplace_back(new Texture("./resources/casuta/textures/plate_brown.png"));
	casutaTexturi.emplace_back(new Texture("./resources/casuta/textures/plate_brown.png"));
	casutaTexturi.emplace_back(new Texture("./resources/casuta/textures/plate_brown.png"));
	casutaTexturi.emplace_back(new Texture("./resources/casuta/textures/plaster_white.png"));
	casutaTexturi.emplace_back(new Texture("./resources/casuta/textures/window.png"));
	casutaTexturi.emplace_back(new Texture("./resources/casuta/textures/window.png"));

	//casuta 2
	std::vector<Mesh*> casuta2 = casuta;
	std::vector<Texture*> casuta2T = casutaTexturi;
	Transform transform2(glm::vec3(-30.0f, 0.1f, -13.0f), glm::vec3(0, 0, 0), glm::vec3(1.5f, 1.5f, 1.5f));
	Shader* casuta2S = new Shader("./resources/shaders/basicShader");

	//casuta 3
	std::vector<Mesh*> casuta3 = casuta;
	std::vector<Texture*> casuta3T = casutaTexturi;
	Transform transform3(glm::vec3(-45.0f, 0.1f, 30.0f), glm::vec3(0, 1.6f, 0), glm::vec3(1.5f, 1.5f, 1.5f));
	Shader* casuta3S = new Shader("./resources/shaders/basicShader");


	//casuta 3
	std::vector<Mesh*> casuta4 = casuta;
	std::vector<Texture*> casuta4T = casutaTexturi;
	Transform transform4(glm::vec3(45.0f, 0.1f, -30.0f), glm::vec3(0, -1.6f, 0), glm::vec3(1.5f, 1.5f, 1.5f));
	Shader* casuta4S = new Shader("./resources/shaders/basicShader");

	while (!display.IsClosed())
	{
		display.Clear(0.0f, 0.15f, 0.3f);

		//MainRoad
		roadT->Bind(0);
		roadS->Bind();
		roadS->Update(transformR, camera);
		roadM->Draw();

		//SecondRoad
		roadT2->Bind(0);
		roadS2->Bind();
		roadS2->Update(transformR2, camera);
		roadM2->Draw();

		//ThirdRoad
		roadT3->Bind(0);
		roadS3->Bind();
		roadS3->Update(transformR3, camera);
		roadM3->Draw();

		//tree
		treeT->Bind(0);
		treeS->Bind();
		treeS->Update(transformT, camera);
		treeM->Draw();

		//tree2
		treeT2->Bind(0);
		treeS2->Bind();
		treeS2->Update(transformT2, camera);
		treeM2->Draw();

		//tree3
		treeT3->Bind(0);
		treeS3->Bind();
		treeS3->Update(transformT3, camera);
		treeM3->Draw();

		//tree4
		treeT4->Bind(0);
		treeS4->Bind();
		treeS4->Update(transformT4, camera);
		treeM4->Draw();
		
		//thuja
		thujaT->Bind(0);
		thujaS->Bind();
		thujaS->Update(transformThuja, camera);
		thujaM->Draw();

		//thuja2
		thujaT2->Bind(0);
		thujaS2->Bind();
		thujaS2->Update(transformThuja2, camera);
		thujaM2->Draw();

		//thuja3
		thujaT3->Bind(0);
		thujaS3->Bind();
		thujaS3->Update(transformThuja3, camera);
		thujaM3->Draw();

		//thuja4
		thujaT4->Bind(0);
		thujaS4->Bind();
		thujaS4->Update(transformThuja4, camera);
		thujaM4->Draw();

		//thuja5
		thujaT5->Bind(0);
		thujaS5->Bind();
		thujaS5->Update(transformThuja5, camera);
		thujaM5->Draw();

		//thuja6
		thujaT6->Bind(0);
		thujaS6->Bind();
		thujaS6->Update(transformThuja6, camera);
		thujaM6->Draw();

		//thuja7
		thujaT7->Bind(0);
		thujaS7->Bind();
		thujaS7->Update(transformThuja7, camera);
		thujaM7->Draw();

		//thuja8
		thujaT8->Bind(0);
		thujaS8->Bind();
		thujaS8->Update(transformThuja8, camera);
		thujaM8->Draw();

		//thuja9
		thujaT9->Bind(0);
		thujaS9->Bind();
		thujaS9->Update(transformThuja9, camera);
		thujaM9->Draw();

		//thuja10
		thujaT10->Bind(0);
		thujaS10->Bind();
		thujaS10->Update(transformThuja10, camera);
		thujaM10->Draw();

		//thuja11
		thujaT11->Bind(0);
		thujaS11->Bind();
		thujaS11->Update(transformThuja11, camera);
		thujaM11->Draw();

		//wolf
		wolfT->Bind(0);
		wolfS->Bind();
		wolfS->Update(transformW, camera);
		wolfM->Draw();

		//wolf2
		wolfT2->Bind(0);
		wolfS2->Bind();
		wolfS2->Update(transformW2, camera);
		wolfM2->Draw();
		
		//wolf3
		wolfT3->Bind(0);
		wolfS3->Bind();
		wolfS3->Update(transformW3, camera);
		wolfM3->Draw();

		//wolf4
		wolfT4->Bind(0);
		wolfS4->Bind();
		wolfS4->Update(transformW4, camera);
		wolfM4->Draw();

		//wolf5
		wolfT5->Bind(0);
		wolfS5->Bind();
		wolfS5->Update(transformW5, camera);
		wolfM5->Draw();
		
		//deer
		deerT->Bind(0);
		deerS->Bind();
		deerS->Update(transformD, camera);
		deer->Draw();

		//deer2
		deerT2->Bind(0);
		deerS2->Bind();
		deerS2->Update(transformD2, camera);
		deer2->Draw();

		//deer3
		deerT3->Bind(0);
		deerS3->Bind();
		deerS3->Update(transformD3, camera);
		deer3->Draw();
		
		//casuta
		for (int i = 0; i < casutaTexturi.size(); i++)
		{
			casutaTexturi[i]->Bind(0);
			casutaS->Bind();
			casutaS->Update(transformC, camera);
			casuta[i]->Draw();
		}

		//casuta2
		for (int i = 0; i < casuta2.size(); i++)
		{
			casuta2T[i]->Bind(0);
			casuta2S->Bind();
			casuta2S->Update(transform2, camera);
			casuta2[i]->Draw();
		}

		//casuta3
		for (int i = 0; i < casuta3.size(); i++)
		{
			casuta3T[i]->Bind(0);
			casuta3S->Bind();
			casuta3S->Update(transform3, camera);
			casuta3[i]->Draw();
		}

		//casuta4
		for (int i = 0; i < casuta3.size(); i++)
		{
			casuta4T[i]->Bind(0);
			casuta4S->Bind();
			casuta4S->Update(transform4, camera);
			casuta4[i]->Draw();
		}
		
		// Render skybox
		skybox.Draw(camera.GetView(), camera.GetProjection());

		//Display terrain
		terrain.UpdateThenDraw(camera);

		display.Update();
	}
	return 0;

}